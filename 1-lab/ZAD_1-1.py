import math
from timeit import default_timer as timer


def f1(n):
    s = 0
    for j in range(1, n):
        s = s + 1 / j
    return s


def f2(n):
    s = 0
    for j in range(1, n):
        for k in range(1, n):
            s = s + k / j
    return s


def f3(n):
    s = 0
    for j in range(1, n):
        for k in range(j, n):
            s = s + k / j
    return s


def f4(n):
    s = 0
    for j in range(1, n):       # n
        k = 2
        while k <= n:
            s = s + k / j
            k = k * 2
    return s                    # n = 32   k = 2, 4, 8, 16, 32    log2n


def f5(n):
    s = 0
    k = 2
    while k <= n:
        s = s + 1 / k
        k = k * 2               # log2n
    return s


nn = [200, 400, 800, 1600, 3200]

print("F(1): ")
for n in nn:
    start = timer()
    f1(n)
    stop = timer()
    Tn = stop - start
    Fn = n
    print(n, Tn, Fn / Tn)
print()

print("F(2): ")
for n in nn:
    start = timer()
    f2(n)
    stop = timer()
    Tn = stop - start
    Fn = n*n
    print(n, Tn, Fn / Tn)
print()

print("F(3): ")
for n in nn:
    start = timer()
    f3(n)
    stop = timer()
    Tn = stop - start
    Fn = n*n
    print(n, Tn, Fn / Tn)
print()

print("F(4): ")
for n in nn:
    start = timer()
    f4(n)
    stop = timer()
    Tn = stop - start
    Fn = n*math.log(n,2)
    print(n, Tn, Fn / Tn)
print()


print("F(5): ")
for n in nn:
    start = timer()
    f5(n)
    stop = timer()
    Tn = stop - start
    Fn = math.log(n,2)
    print(n, Tn, Fn / Tn)
print()


#                           ODPOWIEDZI


# F(1):
# 200 7.669999999999899e-05 2607561.929595862
# 400 0.00036440000000000083 1097694.8408342456
# 800 0.00034879999999999634 2293577.9816514
# 1600 0.00038849999999999996 4118404.118404119
# 3200 0.0012150999999999967 2633528.104682749
#
# F(2):
# 200 0.011152200000000001 3586736.249349904
# 400 0.054795399999999994 2919953.1347521874
# 800 0.1863627 3434163.596041483
# 1600 0.8846483999999999 2893805.0416414025
# 3200 3.3090495000000004 3094544.218815705
#
# F(3):
# 200 0.005166399999999349 7742335.0882635955
# 400 0.03403020000000012 4701706.131612492
# 800 0.1104781000000008 5793003.319209829
# 1600 0.41686089999999965 6141137.2474607285
# 3200 1.5218240000000005 6728767.584162161
#
# F(4):
# 200 0.0005334000000001282 2866087.810188559
# 400 0.0016162999999993488 2139171.2404326443
# 800 0.0036013000000005846 2142305.542947971
# 1600 0.00807779999999969 2108268.328460746
# 3200 0.01616139999999966 2305514.361830034
#
# F(5):
# 200 5.5999999997169425e-06 1364974.3196716234
# 400 4.39999999990448e-06 1964512.7704459943
# 800 5.999999999950489e-06 1607309.3649757174
# 1600 1.0300000000462717e-05 1033384.0960482098
# 3200 7.5999999999964984e-06 1532086.3407605384

# inne funkcje czasu:

# Fn=math.log(n,2)
# Fn=n
# Fn=100*n
# Fn=n*math.log(n,2)
# Fn=n*n
