import math
from timeit import default_timer as timer
import random

def program2(n,m):

    # przykład
    # [1,0,1,0,0   przy pierwszej iteracji histogramem zawsze będzie 1 wiersz zatem go przepisujemy  1 0 1 0 0
    #  1 0 1 1 1    2 0 2 1 1
    #  1 1 1 1 1    3 1 3 2 2
    #  1 0 0 1 0]   4 0 0 3 0
    histogram = []
    maximum = 0;
    for i in range(n):
        if i == 0:
            for j in range(n):
                histogram.append(m[i][j])
        else:
            for j in range(n):
                if m[i][j] != 0:
                    histogram[j] += 1
                else:
                    histogram[j] = 0

        histogram_max = max(histogram)
        for i in range(1,histogram_max + 1):
            actual_max = 0;
            for j in range(n):
                if histogram[j] >= i:
                    actual_max += i
                else:
                    actual_max = 0
                if actual_max > maximum :
                    maximum = actual_max

    return maximum;




# 4
# m = [[0,1,0],[1,1,1],[1,1,0]]

# 4
# m = [[1,0,1,0],[1,0,1,1],[1,1,1,1],[1,0,0,1]]

# 6
# m = [[0,0,0,0,0],[1,0,1,0,1],[1,0,1,1,1],[1,1,1,1,1],[1,0,0,1,1]]

# 3
# m = [[1,1,1],[1,1,1],[1,1,1]]

# 8
# m = [[1,1,1,1],[0,0,0,0],[1,1,1,1],[1,1,1,1]]

# print(program2(4,m))


# generowanie macierzy
def generate(n):
    m = []
    for i in range(n):
        row =[]
        for j in range(n):
            x = random.randint(0,1);
            row.append(x)
        m.append(row)
    print(m)
    return m

nn = [10,100,1000]
for n in nn:
    start = timer()
    m = generate(n)
    program2(n,m)
    stop = timer()
    Tn = stop - start
    Fn = n**2
    print(n, Tn, Fn / Tn)