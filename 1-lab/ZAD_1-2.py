import random
from timeit import default_timer as timer



# 0 0 0 1
# 1 1 0 0
# 1 1 0 0 
# 1 0 0 0 

def Losowa(length):
    M = []
    temp = []
    for i in range(length):
        for j in range(length):
            temp.append(random.randint(0,1))
        M.append(temp)
        temp = []
    return M

def List_0_and_1(length, number):
    M = []
    temp = []
    for i in range(length):
        for j in range(length):
            temp.append(number)
        M.append(temp)
        temp = []
    return M

def NAIWNY(n, M):
    maks = 0
    for x1 in range(0, n):
        for y1 in range(0, n):
            for x2 in range(n - 1, x1 - 1, -1):         # n
                for y2 in range(n - 1, y1 - 1, -1):     # n
                    lokalMaks = 0
                    for x in range(x1, x2 + 1):
                        for y in range(y1, y2 + 1):
                            lokalMaks += M[x][y]
                    if lokalMaks == (x2 - x1 + 1) * (y2 - y1 + 1) and lokalMaks > maks:
                        maks = lokalMaks
    return maks


def DYNAMICZNY(n, M):
    maks = 0
    #  utworz tablicę kwadratową MM rozmiaru n
    #  i wypełnij ją zerami
    MM = []
    temp = []
    for i in range(n):
        for j in range(n):
            temp.append(0)
        MM.append(temp)
        temp = []
    for y in range(0, n):
        for x1 in range(0, n):
            iloczyn = 1
            for x2 in range(x1, n):
                iloczyn *= M[x2][y]
                MM[x1][x2] = iloczyn * (x2 - x1 + 1 + MM[x1][x2])
                if MM[x1][x2] > maks:
                    maks = MM[x1][x2]
    return maks


def CZULY(n, M):
    maks = 0
    for x1 in range(0, n):
        for y1 in range(0, n):
            x2 = x1
            ymaks = n - 1
            while (x2 < n) and (M[x2][y1] == 1):
                y2 = y1
                while (y2 < ymaks + 1) and (M[x2][y2] == 1):
                    y2 += 1
                ymaks = y2 - 1
                lokalMaks = (x2 - x1 + 1) * (ymaks - y1 + 1)
                if lokalMaks > maks:
                    maks = lokalMaks
                x2 += 1
    return maks


# print(NAIWNY(length, M[0]))
# print(DYNAMICZNY(length, M[0]))
# print(CZULY(length, M[0]))


length = 1
print("NAIWNY")
for i in range(3):
    suma = 0
    M = [List_0_and_1(length, 0), List_0_and_1(length, 1), Losowa(length), Losowa(length)]
    print("Length: ", length)
    for m in M:
        start = timer()
        NAIWNY(length, m)
        stop = timer()
        Tn = stop - start
        Fn = length    
        # Fn = pow(length, 6)                        
        suma = suma + Fn / Tn
        print(NAIWNY(length, m), Tn, Fn / Tn)
    length = length * 2
    print("Średnia :: ", round(suma / 4, 3))
    print()
print("--------------------------------")
print()


length = 1
print("DYNAMICZNY")
for i in range(3):
    M = [List_0_and_1(length, 0), List_0_and_1(length, 1), Losowa(length), Losowa(length)]
    print("Length: ", length)
    suma = 0
    for m in M:
        start = timer()
        DYNAMICZNY(length, m)
        stop = timer()
        Tn = stop - start
        Fn = pow(length, 1) # Fn = pow(lenght, 3) + pow(lenght, 2)
        suma = suma + Fn / Tn
        print(DYNAMICZNY(length, m), Tn, Fn / Tn)
    length = length * 2
    print("Średnia :: ", round(suma / 4, 3))
    print()
print("--------------------------------")
print()

length = 1
print("CZULY")
for i in range(3):
    suma = 0
    M = [List_0_and_1(length, 0), List_0_and_1(length, 1), Losowa(length), Losowa(length)]
    print("Length: ", length)
    for m in M:
        start = timer()
        CZULY(length, m)
        stop = timer()
        Tn = stop - start
        Fn = length # pow(lenth, 4)
        suma = suma + Fn / Tn
        print(CZULY(length, m), Tn, Fn / Tn)
    length = length * 2
    print("Średnia :: ", round(suma / 4, 3))
    print()
print("--------------------------------")
print()